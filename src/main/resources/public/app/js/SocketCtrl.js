class SocketCtrl {

  socket;
  stompClient;

  constructor() {
    console.log("SocketCtrl inited");
    this.socket = new WebSocket("ws://localhost:8080/wsapp");
    this.stompClient = Stomp.over(this.socket);
    this.stompClient.connect({}, this.onConnected.bind(this));
  }

  onConnected(frame) {
    console.log('Connected: ' + frame);
    this.stompClient.subscribe('/topic/greetings', function (greeting) {
      console.log("recive message");
      console.log(greeting);
        
    });
    this.stompClient.subscribe('/topic/department/1/window/status', this.onWindowStatusChanged.bind(this));
  }

  onWindowStatusChanged(message) {
    console.log("recive message");
    console.log(message);
  }


  Send(path, msg) {
    this.stompClient.send(path, {}, JSON.stringify(msg));
  }


}