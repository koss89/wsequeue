class App {
  initctrls = [
    'SocketCtrl',
  ];


  ctrls = {};
  constructor() {
    this.initctrls.forEach(item => {
      let obj = eval("new " + item + "()");
      this.ctrls[item] = obj;
    });
    console.log("App Started!");
  }
}

let app = new App();