package com.ws.wsserver.ctrls;

import com.ws.wsserver.services.WindowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.Map;

@Controller
public class WindowCtrl {

    @Autowired
    WindowService windowService;


    Logger logger = LoggerFactory.getLogger(WindowCtrl.class);

    @MessageMapping("/reg-window")
    public void regWindow(Map message) throws Exception {
        logger.trace(message.toString());
        windowService.addWindow(message);
    }



}
