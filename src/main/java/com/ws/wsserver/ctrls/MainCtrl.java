package com.ws.wsserver.ctrls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.Map;


@Controller
public class MainCtrl {

    Logger logger = LoggerFactory.getLogger(MainCtrl.class);

    @MessageMapping("/")
    @SendTo("/topic/greetings")
    public Map greeting(Map message) throws Exception {
        this.logger.trace(message.toString());
        return message;
    }

}
