package com.ws.wsserver.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class WindowService {

    @Autowired
    private SimpMessagingTemplate template;

    private Collection<Map> activeWindows = Collections.emptyList();

    public Collection<Map> getActiveWindows() {
        return activeWindows;
    }

    public void addWindow(Map window) {
        if(!activeWindows.contains(window)) {
            activeWindows.add(window);
        }
    }

    public void removeWindow(Map window) {
        if(activeWindows.contains(window)) {
            activeWindows.remove(window);
        }
    }

    public void windowStateChange(Map window, String status) {
        Map message = new HashMap();
        message.put("window", window);
        message.put("status", status);
        this.template.convertAndSend("/topic/window/status-changed", message);
        this.template.convertAndSend("/topic/window/"+window.get("id")+"/status", message);
        this.template.convertAndSend("/topic/department/"+window.get("iddepartment")+"/window/status", message);
    }

    public void acceptToWindow(Map window, Map vopros) {
        this.template.convertAndSend("/topic/window/accept", vopros);
        this.template.convertAndSend("/topic/window/"+window.get("id")+"/accept", vopros);
        this.template.convertAndSend("/topic/department/"+window.get("iddepartment")+"/window/accept", vopros);
    }

    public void acceptNext(Map vopros) {
        Map window = (Map) vopros.get("window");
        acceptToWindow(window, vopros);
    }




}
