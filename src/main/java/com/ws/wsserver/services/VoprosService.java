package com.ws.wsserver.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class VoprosService {

    @Autowired
    WindowService windowService;
    @Autowired
    QueueService queueService;

    public void changeVorosStatus(Map vopros, String status) {
        Map window = (Map) vopros.get("window");
        String department = (String) vopros.get("department");
        if(status.equals("COMPLETE")) {
            windowService.windowStateChange(window, "FREE");
            queueService.removeAndGoNext(vopros);
        } else if(status.equals("NOTCOME")) {
            windowService.windowStateChange(window, "FREE");
            queueService.removeAndGoNext(vopros);
        } else if(status.equals("LOOK")) {
            windowService.windowStateChange(window, "INUSE");
        }
    }
}
