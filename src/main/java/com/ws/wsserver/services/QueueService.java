package com.ws.wsserver.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class QueueService {

    @Autowired
    WindowService windowService;

    private Collection<Map> queue = Collections.emptyList();

    public void addToQueue(Map vopros) {
        queue.add(vopros);
    }

    public void removeFromQueue(Map vopros) {
        queue.remove(vopros);
    }

    public Collection<Map> getDepartmentQueue(String department) {
        return queue.parallelStream().filter(el -> el.get("department").equals(department)).collect(Collectors.toList());
    }

    public void FillQueue(Collection queue) {
        this.queue = queue;
    }

    public void goNext(String department) {
        var departmentQueue = getDepartmentQueue(department);
        departmentQueue.stream().findFirst().ifPresent(vopros -> windowService.acceptNext(vopros));
    }
    public void removeAndGoNext(Map oldVopros) {
        String department = (String) oldVopros.get("department");
        removeFromQueue(oldVopros);
        goNext(department);
    }
}
